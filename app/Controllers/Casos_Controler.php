<?php

namespace App\Controllers;

use App\Models\Casos;
use App\Models\PropiedadIntelectual;
use App\Models\Oficinas;
use App\Models\Estatus;
use App\Models\RequerimientoUsuario;
use App\Models\Auditoria_sistema_Model;
use App\Models\Ubi_Admini_Model;
use App\Models\Seguimientos;
use App\Models\Casos_remitidos_Model;
use App\Models\Registro_cgr_Model;
use App\Models\Casos_denuncias_Model;
use App\Models\Documentos_casos_Model;
use App\Models\NizaClasses;
use VARIANT;

class Casos_Controler extends BaseController
{
	//Metodo que muestra la vista de los casos
	public function casos()
	{
		$casoModel = new Documentos_casos_Model();
		if ($this->session->get('logged')) {
			$direccionesModel = new Ubi_Admini_Model();
			//Obtenemos las direcciones  para mostrarlos en el modal
			unset($query);
			$query = $direccionesModel->listar_direcciones_administrativas();
			$direccionesopt = '';
			if (isset($query)) {
				foreach ($query->getResult() as $row) {
					$direccionesopt .= '<option value="' . $row->id . '">' . ucfirst(strtolower($row->descripcion)) . '</option>';
				}
			} else {
				$direccionesopt .= '<option value="NULL">Sin estatus</option>';
			}
			$data["mensaje"] = '';
			$data["direcciones"] = $direccionesopt;
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('casos/content', $data);
			echo view('template/footer');
			//echo view('administrador/usuarios/footer.php');
			echo view('/casos/footer_casos.php');
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo que muestra la vista de agregar casos
	public function vista_Agregar_caso()
	{
		if ($this->session->get('logged')) {
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('casos/agregar_caso');
			echo view('template/footer');
			echo view('casos/footer_agregar_caso');
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo para generar un nuevo caso
	public function nuevoCaso()
	{
		$casoModel = new Casos();
		$tipoPIModel = new PropiedadIntelectual();
		$oficina = new Oficinas();
		$reqModel = new RequerimientoUsuario();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$segModel = new Seguimientos();
		$Registro_cgr_Model = new Registro_cgr_Model();
		$Casos_denuncias = new Casos_denuncias_Model();
		//Arreglo para añadir el nuevo caso
		$newCase = array();
		//Arreglo de direccion de casos
		$dirCaso = array();
		//Arreglo con el tipo de propiedad intelectual
		$tipoPI = array();
		//Arreglo para el requerimiento del usuario
		$reqUsuario = array();
		if ($this->session->get('logged') and $this->request->isAJAX()) {
			//Obtenemos los datos del formulario
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			//llenamos los datos iniciales del caso
			$newCase["casofec"]     = $datos["date-entry"];
			$newCase["casoced"]     = $datos["person-id"];
			$newCase["caso_nacionalidad"]     = $datos["nacionalidad"];
			$newCase["casonom"]     = $datos["person-name"];
			$newCase["casoape"]     = $datos["person-lastname"];
			$newCase["casotel"]     = $datos["telephone"];
			$newCase["idest"]       = 1;
			$newCase["idrrss"]      = $datos["social_network"];
			$newCase["idusuopr"]    = $this->session->get('iduser');
			$newCase["estadoid"]    = $datos["state"];
			$newCase["municipioid"] = $datos["county"];
			$newCase["sexo"] = $datos["sexo"];
			$newCase["parroquiaid"] = $datos["town"];
			$newCase["ofiid"]       = $datos["office"];
			$newCase["casodesc"]    = $datos["user-requirement"];
			$newCase["id_tipo_atencion"]    = $datos["tipo-atencion-usu"];
			$newCase["tipo_beneficiario"]    = $datos["tipo_beneficiario"];
			$newCase["direccion"]    = $datos["direccion"];
			$newCase["correo"]    = $datos["correo"];
			$newCase["ente_adscrito_id"]    = $datos["ente_adscrito"];
			$bandera_cgr["bandera_cgr"]    = $datos["bandera_cgr"];
			$bandera_denuncia["bandera_denuncia"]    = $datos["bandera_denuncia"];
			$pi_type = $datos["pi-type"];
			if (empty($datos["record-work"])) {
				$newCase["casonumsol"] = 'No Aplica';
			} else {
				$newCase["casonumsol"] = $datos["record-work"];
			}
			//Realizamos la insercion en la tabla
			$query_insertar_caso = $casoModel->insertarNuevoCaso($newCase);
			if (isset($query_insertar_caso)) {
				//Obtenemos el id insertado
				$_obtener_utimo_id = $casoModel->obtener_utimo_id();
				//Armamos el arreglo para insertar el tipo de propiedad intelectual del caso
				if (empty($_obtener_utimo_id->getResult())) {
					$tipoPI[] = '0';
				} else {
					foreach ($_obtener_utimo_id->getResult() as $fila) {
						$tipoPI['idcaso']      = $fila->ultimo_id;
						$tipoPI['idtippropint']  = $pi_type;
					}
				}
				$repuesta[] = '0';
				//VERIFICAMOS SI ES UN  CASO DE ASESORIA PARA HACER LA INSERCION
				if ($bandera_cgr["bandera_cgr"] == 'true') {
					$cgr['competencia_cgr']  = $datos["competencia_crg"];
					$cgr['asume_cgr']      = $datos["asume_crg"];
					$cgr['id_caso']      = $tipoPI['idcaso'];
					//Hacemos la insercion
					$query_Registro_cgr_Model = $Registro_cgr_Model->insertarRegistro_cgr($cgr);
					//verificacion que se hiso la insercion del cgr
					if (isset($query_Registro_cgr_Model)) {
						//si se hiso la insercion del cgr , hacemos la insercion en el  tipo de propiedad intelectual para el  caso
						$query_tipopimodel = $tipoPIModel->insertarTipoPICaso($tipoPI);
						if (isset($query_tipopimodel)) {
							$repuesta['mensaje']      = 1;
							$repuesta['idcaso']  = $fila->ultimo_id;
							$auditoria['audi_user_id']   = session('iduser');
							$auditoria['audi_accion']   = 'INGRESO UN NUEVO CASO Nª' . $tipoPI['idcaso'];
							$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
							return json_encode($repuesta);
						} else {
							$repuesta['mensaje'] = 2;
							return json_encode($repuesta);
						}
					}
				}
				//VERIFICAMOS SI ES UN  CASO ES DE DENUNCIA PARA HACER LA INSERCION
				if ($bandera_denuncia["bandera_denuncia"]  == 'true') {
					$denuncia['denu_afecta_persona']  = $datos["option_personal"];
					$denuncia['denu_afecta_comunidad']      = $datos["option_comunidad"];
					$denuncia['denu_afecta_terceros']  = $datos["option_terceros"];
					$denuncia['denu_fecha_hechos']      = $datos["fecha_hechos"];
					$denuncia['denu_involucrados']  = $datos["denu_involucrados"];
					$denuncia['denu_instancia_popular']      = $datos["nombre_instancia"];
					$denuncia['denu_rif_instancia']  = $datos["rif_instancia"];
					$denuncia['denu_ente_financiador']      = $datos["ente_financiador"];
					$denuncia['denu_nombre_proyecto']  = $datos["nombre_proyecto"];
					$denuncia['denu_monto_aprovado']      = $datos["monto_aprovado"];
					$denuncia['denu_id_caso']      = $tipoPI['idcaso'];
					//Hacemos la insercion
					$query_Casos_denuncias = $Casos_denuncias->insertarCasos_Denuncias($denuncia);
					//verificacion que se hiso la insercion de los casos de denuncias
					if (isset($query_Casos_denuncias)) {
						//si se hiso la insercion del cgr , hacemos la insercion en el  tipo de propiedad intelectual para el  caso
						$query_tipopimodel = $tipoPIModel->insertarTipoPICaso($tipoPI);
						if (isset($query_tipopimodel)) {
							$repuesta['mensaje']      = 1;
							$repuesta['idcaso']  = $fila->ultimo_id;
							$auditoria['audi_user_id']   = session('iduser');
							$auditoria['audi_accion']   = 'INGRESO UN NUEVO CASO Nª' . $tipoPI['idcaso'];
							$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
							return json_encode($repuesta);
						} else {
							$repuesta['mensaje'] = 2;
							return json_encode($repuesta);
						}
					}
				} else {
					$query_TipoPICaso = $tipoPIModel->insertarTipoPICaso($tipoPI);
					if (isset($query_TipoPICaso)) {
						$repuesta['mensaje']      = 1;
						$repuesta['idcaso']  = $fila->ultimo_id;
						$auditoria['audi_user_id']   = session('iduser');
						$auditoria['audi_accion']   = 'INGRESO UN NUEVO CASO Nª' . $tipoPI['idcaso'];
						$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
						return json_encode($repuesta);
					} else {
						$repuesta['mensaje'] = 2;
						return json_encode($repuesta);
					}
				}
			}
			//}
		} else {

			return redirect()->to('/');
		}
	}
	//Metodo para ElIMINAR  UN CASO 
	public function eliminar_Caso()
	{
		$casoModel = new Casos();
		$tipoPIModel = new PropiedadIntelectual();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$oficina = new Oficinas();
		$reqModel = new RequerimientoUsuario();
		$segModel = new Seguimientos();
		if ($this->session->get('logged') and $this->request->isAJAX()) {
			//Obtenemos los datos del formulario
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			//llenamos los datos iniciales del caso
			$newCase["idcaso"]    = $datos["idcaso"];
			$newCase["borrado"]    = $datos["borrado"];

			//Realizamos la actualizacion en la tabla
			$query_actualizar_caso = $casoModel->actualizarCaso($newCase);
			if (isset($query_actualizar_caso)) {
				$auditoria['audi_user_id']   = session('iduser');
				$auditoria['audi_accion']   = 'ElIMINO EL CASO Nº' . $datos["idcaso"];
				$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				$mensaje = 1;
				return json_encode($mensaje);
			} else {
				$mensaje = 2;
				return json_encode($mensaje);
			}
		} else {
			return redirect()->to('/');
		}
	}

	//Metodo para ACTUALIZAR UN CASO 
	public function actualizarCaso()
	{
		$casoModel = new Casos();
		$tipoPIModel = new PropiedadIntelectual();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$oficina = new Oficinas();
		$reqModel = new RequerimientoUsuario();
		$segModel = new Seguimientos();
		$Registro_cgr_Model = new Registro_cgr_Model();
		$Casos_denuncias = new Casos_denuncias_Model();
		//Arreglo para añadir el nuevo caso
		$newCase = array();
		$denuncia = array();
		//Arreglo de direccion de casos
		$dirCaso = array();
		//Arreglo con el tipo de propiedad intelectual
		$tipoPI = array();
		//Arreglo para el requerimiento del usuario
		$reqUsuario = array();
		if ($this->session->get('logged') and $this->request->isAJAX()) {
			//Obtenemos los datos del formulario
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$newCase["id_tipo_atencion"]    = $datos["tipo-atencion-usu"];
			$tipoPI["idtippropint"]    = $datos["pi-type"];
			$tipoPI["idcaso"]    = $datos["idcaso"];
			//llenamos los datos iniciales del caso
			if ($newCase["id_tipo_atencion"] !== 5 && $newCase["id_tipo_atencion"] !== 1) {
				$newCase["idcaso"]    = $datos["idcaso"];
				$newCase["casofec"]     = $datos["date-entry"];
				$newCase["casoced"]     = $datos["person-id"];
				$newCase["caso_nacionalidad"]     = $datos["nacionalidad"];
				$newCase["casonom"]     = $datos["person-name"];
				$newCase["casoape"]     = $datos["person-lastname"];
				$newCase["casotel"]     = $datos["telephone"];
				$newCase["idest"]       = 1;
				$newCase["idrrss"]      = $datos["social_network"];
				$newCase["idusuopr"]    = $this->session->get('iduser');
				$newCase["estadoid"]    = $datos["state"];
				$newCase["municipioid"] = $datos["county"];
				$newCase["sexo"] = $datos["sexo"];
				$newCase["parroquiaid"] = $datos["town"];
				$newCase["ofiid"]       = $datos["office"];
				$newCase["casodesc"]    = $datos["user-requirement"];
				$newCase["id_tipo_atencion"]    = $datos["tipo-atencion-usu"];
				$newCase["tipo_beneficiario"]    = $datos["tipo_beneficiario"];
				$newCase["direccion"]    = $datos["direccion"];
				$newCase["correo"]    = $datos["correo"];
				$newCase["ente_adscrito_id"]    = $datos["ente_adscrito_id"];
				//$newCase["campos_modificados"]    = $datos["campos_modificados"];
				if (empty($datos["record-work"])) {
					$newCase["casonumsol"] = 'No Aplica';
				} else {
					$newCase["casonumsol"] = $datos["record-work"];
				}
				//REALIZAMOS LA ACTUALIZACION EN LA TABLA
				$query_actualizar_caso = $casoModel->actualizarCaso($newCase);

				$auditoria['audi_user_id']   = session('iduser');
				$auditoria['audi_accion']   = 'LOS SIGUIENTES CAMPOS  DE EL CASO Nº' . $datos["idcaso"] . ' ' . 'FUERON MODIFICADOS  :' . ' ' . $datos["campos_modificados"];
				$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
			} else if ($newCase["id_tipo_atencion"] == 5) {
				//SI ES UNA DENUNCIA ARMAMOS EL ARRAY 
				$denuncia["denu_afecta_persona"]    = $datos["denu_afecta_persona"];
				$denuncia["denu_afecta_comunidad"]    = $datos["denu_afecta_comunidad"];
				$denuncia["denu_afecta_terceros"]    = $datos["denu_afecta_terceros"];
				$denuncia["denu_fecha_hechos"]    = $datos["denu_fecha_hechos"];
				$denuncia["denu_involucrados"]    = $datos["denu_involucrados"];
				$denuncia['denu_instancia_popular']      = $datos["denu_instancia_popular"];
				$denuncia["denu_rif_instancia"]    = $datos["denu_rif_instancia"];
				$denuncia['denu_ente_financiador']      = $datos["denu_ente_financiador"];
				$denuncia["denu_nombre_proyecto"]    = $datos["denu_nombre_proyecto"];
				$denuncia["denu_monto_aprovado"]    = $datos["denu_monto_aprovado"];
				$denuncia['denu_id_caso']      = $tipoPI['idcaso'];
				$denuncia["denu_borrado"]    = false;
				$idcaso = $tipoPI['idcaso'];
				//BUSCAMOS EL ID EN LA TABLA DE DENUNCIAS , SI NO EXISTE PROCEDEMOS CON EL INSERT
				$query_buscarid_denuncia = $Casos_denuncias->verificar_id_caso_denuncia($idcaso);
				if (empty($query_buscarid_denuncia)) {
					$query_Casos_denuncias = $Casos_denuncias->insertarCasos_Denuncias($denuncia);
					$newCase["idcaso"]    = $datos["idcaso"];
					$newCase["casofec"]     = $datos["date-entry"];
					$newCase["casoced"]     = $datos["person-id"];
					$newCase["caso_nacionalidad"]     = $datos["nacionalidad"];
					$newCase["casonom"]     = $datos["person-name"];
					$newCase["casoape"]     = $datos["person-lastname"];
					$newCase["casotel"]     = $datos["telephone"];
					$newCase["idest"]       = 1;
					$newCase["idrrss"]      = $datos["social_network"];
					$newCase["idusuopr"]    = $this->session->get('iduser');
					$newCase["estadoid"]    = $datos["state"];
					$newCase["municipioid"] = $datos["county"];
					$newCase["sexo"] = $datos["sexo"];
					$newCase["parroquiaid"] = $datos["town"];
					$newCase["ofiid"]       = $datos["office"];
					$newCase["casodesc"]    = $datos["user-requirement"];
					$newCase["id_tipo_atencion"]    = $datos["tipo-atencion-usu"];
					$newCase["tipo_beneficiario"]    = $datos["tipo_beneficiario"];
					$newCase["direccion"]    = $datos["direccion"];
					$newCase["correo"]    = $datos["correo"];
					$newCase["ente_adscrito_id"]    = $datos["ente_adscrito_id"];
					//$newCase["campos_modificados"]    = $datos["campos_modificados"];
					if (empty($datos["record-work"])) {
						$newCase["casonumsol"] = 'No Aplica';
					} else {
						$newCase["casonumsol"] = $datos["record-work"];
					}
					//REALIZAMOS LA ACTUALIZACION EN LA TABLA
					$query_actualizar_caso = $casoModel->actualizarCaso($newCase);
					$auditoria['audi_user_id']   = session('iduser');
					$auditoria['audi_accion']   = 'REGISTRO EN LA TABLA DE denunciaS EL CASO Nº' . $datos["idcaso"];
					$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				} else {
					$newCase["idcaso"]    = $datos["idcaso"];
					$newCase["casofec"]     = $datos["date-entry"];
					$newCase["casoced"]     = $datos["person-id"];
					$newCase["caso_nacionalidad"]     = $datos["nacionalidad"];
					$newCase["casonom"]     = $datos["person-name"];
					$newCase["casoape"]     = $datos["person-lastname"];
					$newCase["casotel"]     = $datos["telephone"];
					$newCase["idest"]       = 1;
					$newCase["idrrss"]      = $datos["social_network"];
					$newCase["idusuopr"]    = $this->session->get('iduser');
					$newCase["estadoid"]    = $datos["state"];
					$newCase["municipioid"] = $datos["county"];
					$newCase["sexo"] = $datos["sexo"];
					$newCase["parroquiaid"] = $datos["town"];
					$newCase["ofiid"]       = $datos["office"];
					$newCase["casodesc"]    = $datos["user-requirement"];
					$newCase["id_tipo_atencion"]    = $datos["tipo-atencion-usu"];
					$newCase["tipo_beneficiario"]    = $datos["tipo_beneficiario"];
					$newCase["direccion"]    = $datos["direccion"];
					$newCase["correo"]    = $datos["correo"];
					$newCase["ente_adscrito_id"]    = $datos["ente_adscrito_id"];
					//$newCase["campos_modificados"]    = $datos["campos_modificados"];
					if (empty($datos["record-work"])) {
						$newCase["casonumsol"] = 'No Aplica';
					} else {
						$newCase["casonumsol"] = $datos["record-work"];
					}
					//REALIZAMOS LA ACTUALIZACION EN LA TABLA
					$query_actualizar_caso = $casoModel->actualizarCaso($newCase);
					// SI EXISTE EL REGISTRO HACEMOS UN UPDATE EN FUNSION DEL ID 
					$denuncia["denu_afecta_persona"]    = $datos["denu_afecta_persona"];
					$denuncia["denu_afecta_comunidad"]    = $datos["denu_afecta_comunidad"];
					$denuncia["denu_afecta_terceros"]    = $datos["denu_afecta_terceros"];
					$denuncia["denu_fecha_hechos"]    = $datos["denu_fecha_hechos"];
					$denuncia["denu_involucrados"]    = $datos["denu_involucrados"];
					$denuncia['denu_instancia_popular']      = $datos["denu_instancia_popular"];
					$denuncia["denu_rif_instancia"]    = $datos["denu_rif_instancia"];
					$denuncia['denu_ente_financiador']      = $datos["denu_ente_financiador"];
					$denuncia["denu_nombre_proyecto"]    = $datos["denu_nombre_proyecto"];
					$denuncia["denu_monto_aprovado"]    = $datos["denu_monto_aprovado"];
					$denuncia['denu_id_caso']      = $tipoPI['idcaso'];
					$denuncia["denu_borrado"]    = false;
					$query_actualizara_Casos_denuncias = $Casos_denuncias->AtualizarCasos_Denuncias($denuncia);
					$auditoria['audi_user_id']   = session('iduser');
					$auditoria['audi_accion']   = 'LOS SIGUIENTES CAMPOS  DE EL CASO Nº' . $datos["idcaso"] . ' ' . 'FUERON MODIFICADOS  :' . ' ' . $datos["campos_modificados"];
					$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				}
				//LUEGO BUSCAMOS EL ID DEL CASO  EN LA TABLA DE CRG PARA HACER EL BORRADO LOGICO SI EXISTE EL REGISTRO
				$query_buscarid_CGR = $Registro_cgr_Model->verificar_id_caso_CGR($idcaso);
				if (!empty($query_buscarid_CGR)) {
					$cgr["borrado_cgr"]    = true;
					$cgr['id_caso']      = $tipoPI['idcaso'];
					$query_buscarid_CGR = $Registro_cgr_Model->AtualizarCasos_cgr($cgr);
				}
				//SI ES UNA ASESORIA ARMAMOS EL ARRAY 
			} else if ($newCase["id_tipo_atencion"] == 1) {
				$idcaso = $tipoPI['idcaso'];
				//BUSCAMOS EL ID EN LA TABLA DE CRG , SI NO EXISTE PROCEDEMOS CON EL INSERT
				$query_buscarid_CGR = $Registro_cgr_Model->verificar_id_caso_CGR($idcaso);
				if (empty($query_buscarid_CGR)) {
					$cgr["competencia_cgr"]    = $datos["competencia_cgr"];
					$cgr["asume_cgr"]    = $datos["asume_cgr"];
					$cgr['id_caso']      = $tipoPI['idcaso'];
					$query_Registro_cgr_Model = $Registro_cgr_Model->insertarRegistro_cgr($cgr);
					$auditoria['audi_user_id']   = session('iduser');
					$auditoria['audi_accion']   = 'REGISTRO EN LA TABLA DE CGR EL CASO Nº' . $datos["idcaso"];
					$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				} else {
					// SI EXISTE EL REGISTRO HACEMOS UN UPDATE EN FUNSION DEL ID 
					$cgr["competencia_cgr"]    = $datos["competencia_cgr"];
					$cgr["asume_cgr"]    = $datos["asume_cgr"];
					$cgr["borrado_cgr"]    = false;
					$cgr['id_caso']      = $tipoPI['idcaso'];
					$query_actualizara_Casos_cgr = $Registro_cgr_Model->AtualizarCasos_cgr($cgr);
					$auditoria['audi_user_id']   = session('iduser');
					$auditoria['audi_accion']   = 'LOS SIGUIENTES CAMPOS DE EL CASO Nº' . $datos["idcaso"] . ' ' . 'FUERON MODIFICADOS  :' . ' ' . $datos["campos_modificados"];
					$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				}
				//LUEGO BUSCAMOS EL ID DEL CASO  EN LA TABLA DE DENUNCIAS PARA HACER EL BORRADO LOGICO SI EXISTE EL REGISTRO
				$query_buscarid_denuncia = $Casos_denuncias->verificar_id_caso_denuncia($idcaso);
				if (!empty($query_buscarid_denuncia)) {
					$denuncia["denu_borrado"]    = true;
					$denuncia['denu_id_caso']      = $tipoPI['idcaso'];
					$query_actualizar_denuncia = $Casos_denuncias->AtualizarCasos_Denuncias($denuncia);
				}
			}
			// // //Hacemos la la actualizacion 
			$query_tipopimodel = $tipoPIModel->ActualizarTipoPICaso($tipoPI);
			if (isset($query_tipopimodel)) {
				// $auditoria['audi_user_id']   = session('iduser');
				// $auditoria['audi_accion']   = 'ACTUALIZO EL CASO Nº' . $datos["idcaso"];
				// $Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				$mensaje = 1;
				return json_encode($mensaje);
			} else {
				$mensaje = 2;
				return json_encode($mensaje);
			}
		} else {
			return redirect()->to('/');
		}
	}

	//Vista de carga de un caso
	public function vercaso($id)
	{
		$casoModel = new Casos();
		$segModel = new Seguimientos();
		$estModel = new Estatus();
		$direccionesModel = new Ubi_Admini_Model();
		//Arreglo para los detalles del caso
		$data = array();
		//TimeLine para los seguimientos
		$tlSeguimientos = '';
		//estatus del caso
		$idEstatusCaso = '';
		if ($this->session->get('logged')) {
			//Consultamos los detalles del caso
			$query = $casoModel->detalleCaso($id);
			if (isset($query)) {
				foreach ($query as $row) {
					$data["idcaso"] = $id;
					$data["nombre"] = ucwords(strtolower($row->casonom) . ' ' . strtolower($row->casoape));
					$data["estado"] = ucfirst(strtolower($row->estadonom));
					$data["municipio"] = ucfirst(strtolower($row->municipionom));
					$data["parroquia"] = ucfirst(strtolower($row->parroquianom));
					$data["casodesc"] = ucfirst(strtolower($row->casodesc));
					$data["correo"] = ucfirst(strtolower($row->correo));
					$data["fecha_caso"] = $row->casofec;
					$data["usuario_operador"] = $row->user_name;
					$data["unidad_administrativa"] = $row->unidad_administrativa;
					$data["direccion"] = $row->direccion;
					$data["correo_beneficiario"] = $row->correo;
					//$idEstatusCaso = $row->idest;
				}
				//Obtenemos los estatus de las llamadas para el select del seguimiento
				unset($query);
				$query = $estModel->estatusLlamadas();
				$estopt = '';
				if (isset($query)) {
					foreach ($query->getResult() as $row) {
						$estopt .= '<option value="' . $row->idestllam . '">' . ucfirst(strtolower($row->estllamnom)) . '</option>';
					}
				} else {
					$estopt .= '<option value="NULL">Sin estatus</option>';
				}
				$data["estatus"] = $estopt;
				//Obtenemos los estatus de los casos para mostrarlos en el modal
				unset($query);
				$estopt = '';
				$query = $estModel->estatusCaso();
				if (isset($query)) {
					foreach ($query->getResult() as $row) {
						if ($row->idest == $idEstatusCaso) {
							$estopt .= '<option selected value="' . $row->idest . '">' . ucfirst(strtolower($row->estnom)) . '</option>';
						} else {
							$estopt .= '<option value="' . $row->idest . '">' . ucfirst(strtolower($row->estnom)) . '</option>';
						}
					}
				} else {
					$estopt .= '<option value="NULL">Sin estatus</option>';
				}
				$data["estatus_llamadas"] = $estopt;
				echo view('template/header');
				echo view('template/nav_bar');
				echo view('casos/ver_caso/content.php', $data);
				echo view('template/footer');
				echo view('casos/ver_caso/footer.php');
			} else {
				return redirect()->to('/404');
			}
		} else {
			return redirect()->to('/');
		}
	}

	//Metodo para ACTUALIZAR UN CASO 
	public function remitirCaso()
	{

		$casoModel = new Casos();
		$caso_remitido = new Casos_remitidos_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		//Arreglo para remitir un caso
		$remitirCase = array();
		if ($this->session->get('logged') and $this->request->isAJAX()) {
			//Obtenemos los datos del formulario
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			//llenamos los datos iniciales del caso
			$remitirCase["casos_id"]    = $datos["id_caso"];
			$remitirCase["direccion_id"]     = $datos["direccion"];
			$nombre_direccion["nombre_direccion"]     = $datos["nombre_direccion"];
			$remitirCase["idusuop"]    = $this->session->get('iduser');
			//Realizamos la Insercion en la tabla
			$repuesta[] = '0';
			$query_remitir_caso = $caso_remitido->remitirCaso($remitirCase);
			if (isset($query_remitir_caso)) {
				$auditoria['audi_user_id']   = session('iduser');
				$auditoria['audi_accion']   = 'REMITIO EL CASO Nº' . $datos["id_caso"] . ' ' . 'A' . ' ' . '(' . ' ' . $nombre_direccion["nombre_direccion"] . ' ' . ')';
				$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
				$repuesta['mensaje']      = 1;
				$repuesta['idcaso']  = $datos["id_caso"];
				return json_encode($repuesta);
			} else {
				$repuesta['mensaje']      = 2;
				return json_encode($repuesta);
			}
		} else {
			return redirect()->to('/');
		}
	}



	//Metodo queo obtiene  los todos los casos disponibles
	public function listar_Casos_Usuarios()
	{
		$model = new Casos();
		$query = $model->obtenerCasos();
		if (empty($query)) {
			$casos = [];
		} else {
			$casos = $query;
		}
		echo json_encode($casos);
	}
	//Metodo queo obtiene  los Ultimos_Casos  disponibles
	public function listar_Ultimos_Casos()
	{
		$iduser = (session('iduser'));
		$model = new Casos();
		$query = $model->obtener_ultimos_casos($iduser);
		if (empty($query)) {
			$ultimos_casos = [];
		} else {
			$ultimos_casos = $query;
		}
		echo json_encode($ultimos_casos);
	}


	//Metodo para subir acrhivos
	public function upload()
	{
		$model = new Casos();
		$id_caso_pdf[0] = '';
		$id_caso_pdf[0] = $_POST['id_caso_pdf'];
		$archivo[] = '';
		$archivo[0] = $_FILES['archivo'];
		$config['allowed_types'] = 'doc|docx|pdf|odt';
		$tamañoMaximo = 3 * 1024 * 1024; // 3MB en bytes
		if ($archivo[0]["name"] != '') {
			$nombreArchivo = $id_caso_pdf[0] . '_' . trim($archivo[0]['name']);
			$nombreArchivo = strtolower($nombreArchivo);
			$archivoTemporal = $archivo[0]['tmp_name'];
			$archivoTemporal = trim($archivoTemporal);
			$ruta = base_url();
			$rutaDestino = WRITEPATH . trim($nombreArchivo);
			$targetDir = WRITEPATH; // Directorio donde se guardarán los archivos subidos
			$targetFile = $targetDir . basename($nombreArchivo);
			$uploadOk = 1;
			$fileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
			// Verificar si el archivo ya existe
			if (file_exists($targetFile)) {
				//echo "El archivo ya existe.";
				$data = 0;
			}
			// Verificar el tamaño del archivo (opcional)
			else if ($archivo[0]["size"] > $tamañoMaximo) {
				//echo "El archivo es demasiado grande.";
				$data = 1;
				return json_encode($data);
			} else if (move_uploaded_file($archivoTemporal, $rutaDestino)) {
				//echo "ARCHIVO CARGADO EXITOSAMENTE.";
				$documentos_casos['docu_id_caso'] = $id_caso_pdf[0];
				$documentos_casos['docu_ruta'] = $nombreArchivo;
				$query_docu_casos = $model->agregar_docu_casos($documentos_casos);
				if (isset($query_docu_casos)) {
					$data = 2;
					return json_encode($data);
				} else {
					echo ('error al enviar a la base de datos ');
				}
			} else {

				$data = 3;
				return json_encode($data);


				//echo "Error al subir el archivo.";
			}
		} else {
			$data = 4;
			return json_encode($data);
			//echo ('vacio');
		}




		// $direccionesModel = new Ubi_Admini_Model();
		// //Obtenemos las direcciones  para mostrarlos en el modal
		// unset($query);
		// $query = $direccionesModel->listar_Ubicacion_Administrativa();
		// $direccionesopt = '';
		// if (isset($query)) {
		// 	foreach ($query->getResult() as $row) {
		// 		$direccionesopt .= '<option value="' . $row->id . '">' . ucfirst(strtolower($row->descripcion)) . '</option>';
		// 	}
		// } else {
		// 	$direccionesopt .= '<option value="NULL">Sin estatus</option>';
		// }
		// $data["direcciones"] = $direccionesopt;
		// $data["mensaje"] = $data["mensaje"];
		// echo view('template/header');
		// echo view('template/nav_bar');
		// echo view('casos/content', $data);
		// echo view('template/footer');
		// //echo view('administrador/usuarios/footer.php');
		// echo view('/casos/footer_casos.php');
	}
}
