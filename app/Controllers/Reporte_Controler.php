<?php

namespace App\Controllers;

use App\Models\Casos;
use App\Models\PropiedadIntelectual;
use App\Models\Oficinas;
use App\Models\Estatus;
use App\Models\RequerimientoUsuario;
use App\Models\Auditoria_sistema_Model;
use App\Models\Ubi_Admini_Model;
use App\Models\Seguimientos;
use App\Models\Casos_remitidos_Model;
use App\Models\Registro_cgr_Model;
use App\Models\Casos_denuncias_Model;
use App\Models\NizaClasses;
use App\Models\Usuarios;



class Reporte_Controler extends BaseController
{
	public function vista_consolidado()
	{
		if ($this->session->get('logged')) {
			$direccionesModel = new Ubi_Admini_Model();
			//Obtenemos las direcciones  para mostrarlos en el modal
			unset($query);
			$query = $direccionesModel->listar_Ubicacion_Administrativa();

			$direccionesopt = '';
			if (isset($query)) {
				foreach ($query->getResult() as $row) {
					$direccionesopt .= '<option value="' . $row->id . '">' . ucfirst(strtolower($row->descripcion)) . '</option>';
				}
			} else {
				$direccionesopt .= '<option value="NULL">Sin estatus</option>';
			}
			$data["direcciones"] = $direccionesopt;
			//Pasamos la tabla como parametro para la vista
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('reportes/general/content', $data);
			echo view('template/footer');
			echo view('reportes/general/footer.php');
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo queo obtiene  los todos los casos disponibles
	public function reporte_consolidado($desde = null, $hasta = null, $tipo_pi = null, $tipo_atencion_usu = null, $sexo = null, $via_atencion = null, $direcciones_caso = null, $tipo_beneficiario = 0)
	{
		$model = new Casos();
		$query = $model->reporte_consolidado($desde, $hasta, $tipo_pi, $tipo_atencion_usu, $sexo, $via_atencion, $direcciones_caso, $tipo_beneficiario);

		if (empty($query)) {
			$casos = [];
		} else {
			$casos = $query;
		}
		echo json_encode($casos);
	}

	public function vista_operador()
	{
		if ($this->session->get('logged')) {
			$direccionesModel = new Ubi_Admini_Model();
			$usuariosModel = new Usuarios();
			//Obtenemos las direcciones  para mostrarlos en el modal
			unset($query);
			$query = $direccionesModel->listar_Ubicacion_Administrativa();
			$query_usuarios = $usuariosModel->getAllUsers();


			$direccionesopt = '';
			$usuarios = '';
			if (isset($query)) {
				foreach ($query->getResult() as $row) {
					$direccionesopt .= '<option value="' . $row->id . '">' . ucfirst(strtolower($row->descripcion)) . '</option>';
				}
			} else {
				$direccionesopt .= '<option value="NULL">Sin estatus</option>';
			}
			if (isset($query_usuarios)) {
				foreach ($query_usuarios->getResult() as $row) {
					$usuarios .= '<option value="' . $row->idusuopr . '">' . ucfirst(strtolower($row->usuopnom . ' ' . $row->usuopape)) . '</option>';
				}
			} else {
				$usuarios .= '<option value="NULL">Sin estatus</option>';
			}




			$data["direcciones"] = $direccionesopt;
			$data["usuarios"] = $usuarios;
			//Pasamos la tabla como parametro para la vista
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('reportes/operador/content.php', $data);
			echo view('template/footer');
			echo view('reportes/operador/footer.php');
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo queo obtiene  los todos los casos disponibles POR USUARIO
	public function reporte_operador($desde = null, $hasta = null, $tipo_pi = null, $tipo_atencion_usu = null, $sexo = null, $via_atencion = null, $direcciones_caso = null, $tipo_beneficiario = 0, $usuarios = null)
	{
		$model = new Casos();
		$idusuopr   = $this->session->get('iduser');
		$query = $model->reporte_operador($desde, $hasta, $tipo_pi, $tipo_atencion_usu, $sexo, $idusuopr, $via_atencion, $direcciones_caso, $tipo_beneficiario, $usuarios);
		if (empty($query)) {
			$casos = [];
		} else {
			$casos = $query;
		}
		echo json_encode($casos);
	}

	public function vista_estadisticas()
	{
		if ($this->session->get('logged')) {
			$model = new Casos();
			$desde = 'null';
			$hasta = 'null';
			//BUSCAMOS LOS CASOS ATENDIDOS POR TIPO BENEFICIARIO USUARIO
			$estadisticas["usuario"] = 0;
			$beneficiario_usuario = $model->contarCasos_Beneficiario_Usuario($desde, $hasta);
			if (!empty($beneficiario_usuario)) {
				for ($i = 0; $i < count($beneficiario_usuario); $i++) {
					if ($beneficiario_usuario[$i]->tipo_beneficiario == "1") {
						$estadisticas["usuario"] = $beneficiario_usuario[$i]->count;
					} else {
						$estadisticas[$beneficiario_usuario[$i]->tipo_beneficiario] = $beneficiario_usuario[$i]->count;
					}
				}
			} else {
				$estadisticas["usuario"] = 0;
			}
			//BUSCAMOS LOS CASOS ATENDIDOS POR TIPO BENEFICIARIO EMPRENDERDOR
			$beneficiario_emprendedor = $model->contarCasos_Beneficiario_Emprendedor($desde, $hasta);
			if (!empty($beneficiario_emprendedor)) {
				for ($i = 0; $i < count($beneficiario_emprendedor); $i++) {
					if ($beneficiario_emprendedor[$i]->tipo_beneficiario == "2") {
						$estadisticas["emprendedor"] = $beneficiario_emprendedor[$i]->count;
					} else {
						$estadisticas[$beneficiario_emprendedor[$i]->tipo_beneficiario] = $beneficiario_emprendedor[$i]->count;
					}
				}
			} else {
				$estadisticas["emprendedor"] = 0;
			}
			if ($estadisticas["emprendedor"] == 0 && $estadisticas["usuario"] == 0) {
				echo view('template/header');
				echo view('template/nav_bar');
				echo view('reportes/estadisticas/error_estadisticas.php');
				echo view('template/footer');
			} else {
				//	BUSCAMOS LOS CASOS ATENDIDOS POR RED SOCIAL
				$query_casos_atendidos = $model->contarCasosAtendidos();
				$estadisticas["Sexo_CorreoElectronico_M"] = 0;
				$estadisticas["Sexo_Llamadatelefonica_M"] = 0;
				$estadisticas["Sexo_Whatsapp_M"] = 0;
				$estadisticas["Sexo_Personal_M"] = 0;
				$estadisticas["Sexo_CorreoElectronico_F"] = 0;
				$estadisticas["Sexo_Llamadatelefonica_F"] = 0;
				$estadisticas["Sexo_Whatsapp_F"] = 0;
				$estadisticas["Sexo_Personal_F"] = 0;
				//BUSCAMOS LOS CASOS ATENDIDOS POR GENERO MASCULINO
				$query_casos_atendidos_Masculino = $model->contarCasosAtendidos_MASCULINO($desde, $hasta);
				if (!empty($query_casos_atendidos_Masculino)) {
					for ($i = 0; $i < count($query_casos_atendidos_Masculino); $i++) {
						if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Correo Electronico") {
							$estadisticas["Sexo_CorreoElectronico_M"] = $query_casos_atendidos_Masculino[$i]->count;
						} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Llamada telefonica") {
							$estadisticas["Sexo_Llamadatelefonica_M"] = $query_casos_atendidos_Masculino[$i]->count;
						} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Whatsapp") {
							$estadisticas["Sexo_Whatsapp_M"] = $query_casos_atendidos_Masculino[$i]->count;
						} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Personal") {
							$estadisticas["Sexo_Personal_M"] = $query_casos_atendidos_Masculino[$i]->count;
						} else {
							$estadisticas[$query_casos_atendidos_Masculino[$i]->red_s_nom] = $query_casos_atendidos_Masculino[$i]->count;
						}
					}
				}

				//BUSCAMOS LOS CASOS ATENDIDOS POR GENERO FEMENINO
				$query_casos_atendidos_Femenino = $model->contarCasosAtendidos_FEMENINO($desde, $hasta);
				if (!empty($query_casos_atendidos_Femenino)) {
					for ($i = 0; $i < count($query_casos_atendidos_Femenino); $i++) {
						if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Correo Electronico") {
							$estadisticas["Sexo_CorreoElectronico_F"] = $query_casos_atendidos_Femenino[$i]->count;
						} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Llamada telefonica") {
							$estadisticas["Sexo_Llamadatelefonica_F"] = $query_casos_atendidos_Femenino[$i]->count;
						} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Whatsapp") {
							$estadisticas["Sexo_Whatsapp_F"] = $query_casos_atendidos_Femenino[$i]->count;
						} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Personal") {
							$estadisticas["Sexo_Personal_F"] = $query_casos_atendidos_Femenino[$i]->count;
						} else {
							$estadisticas[$query_casos_atendidos_Femenino[$i]->red_s_nom] = $query_casos_atendidos_Femenino[$i]->count;
						}
					}
				}
				$estadisticas["Whatsapp"] = 0;
				$estadisticas["CorreoElectronico"] = 0;
				$estadisticas["Llamadatelefonica"] = 0;
				$estadisticas["Personal"] = 0;
				if (empty($query_casos_atendidos)) {
					$estadisticas = [];
				} else {
					for ($i = 0; $i < count($query_casos_atendidos); $i++) {
						if ($query_casos_atendidos[$i]->red_s_nom == "Correo Electronico") {
							$estadisticas["CorreoElectronico"] = $query_casos_atendidos[$i]->count;
						} else if ($query_casos_atendidos[$i]->red_s_nom == "Llamada telefonica") {
							$estadisticas["Llamadatelefonica"] = $query_casos_atendidos[$i]->count;
						} else {
							$estadisticas[$query_casos_atendidos[$i]->red_s_nom] = $query_casos_atendidos[$i]->count;
						}
					}
				}
				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL
				$query = $model->contarCasosPorPI();
				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL GENERO MASCULINO
				$query_casos_MASCULINOS = $model->contarCasos_PI_MACULINO($desde, $hasta);
				$estadisticas["sexo_Marcas_M"] = 0;
				$estadisticas["sexo_Patentes_M"] = 0;
				$estadisticas["sexo_DerechoAutor_M"] = 0;
				$estadisticas["sexo_Indicaciones_Geograficas_M"] = 0;
				for ($i = 0; $i < count($query_casos_MASCULINOS); $i++) {
					if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Derecho de Autor") {
						$estadisticas["sexo_DerechoAutor_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
						$estadisticas["sexo_Indicaciones_Geograficas_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Marcas") {
						$estadisticas["sexo_Marcas_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Patentes") {
						$estadisticas["sexo_Patentes_M"] = $query_casos_MASCULINOS[$i]->count;
					} else {
						$estadisticas[$query_casos_MASCULINOS[$i]->tipo_prop_nombre] = $query_casos_MASCULINOS[$i]->count;
					}
				}

				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL GENERO FEMENINO
				$query_casos_FEMENINOS = $model->contarCasos_PI_FEMENINO($desde, $hasta);
				$estadisticas["sexo_Marcas_F"] = 0;
				$estadisticas["sexo_Patentes_F"] = 0;
				$estadisticas["sexo_DerechoAutor_F"] = 0;
				$estadisticas["sexo_Indicaciones_Geograficas_F"] = 0;
				for ($i = 0; $i < count($query_casos_FEMENINOS); $i++) {
					if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Derecho de Autor") {
						$estadisticas["sexo_DerechoAutor_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
						$estadisticas["sexo_Indicaciones_Geograficas_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Marcas") {
						$estadisticas["sexo_Marcas_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Patentes") {
						$estadisticas["sexo_Patentes_F"] = $query_casos_FEMENINOS[$i]->count;
					} else {
						$estadisticas[$query_casos_FEMENINOS[$i]->tipo_prop_nombre] = $query_casos_FEMENINOS[$i]->count;
					}
				}
				$estadisticas["Marcas"] = 0;
				$estadisticas["Patentes"] = 0;
				$estadisticas["DerechoAutor"] = 0;
				$estadisticas["Indicaciones_Geograficas"] = 0;
				if (empty($query)) {
					$estadisticas = [];
				} else {
					for ($i = 0; $i < count($query); $i++) {
						if ($query[$i]->tipo_prop_nombre == "Derecho de Autor") {
							$estadisticas["DerechoAutor"] = $query[$i]->count;
						} else if ($query[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
							$estadisticas["Indicaciones_Geograficas"] = $query[$i]->count;
						} else {
							$estadisticas[$query[$i]->tipo_prop_nombre] = $query[$i]->count;
						}
					}
					//var_dump($estadisticas);
				}

				// $fecha_desde = $desde;
				// $fecha_hasta = $hasta; // Fecha en formato YYYY-MM-DD
				// // Formatear la fecha al formato dd-mm-yy
				// $fecha_formateada_desde = date("d-m-y", strtotime($fecha_desde));
				// $fecha_formateada_hasta = date("d-m-y", strtotime($fecha_hasta));

				$estadisticas["fecha_desde"] = date("d-m-Y");
				$estadisticas["fecha_hasta"] = date("d-m-Y");

				//Pasamos la tabla como parametro para la vista
				echo view('template/header');
				echo view('template/nav_bar');
				echo view('reportes/estadisticas/content.php', $estadisticas);
				echo view('template/footer');
				echo view('reportes/estadisticas/footer.php');
			}
		} else {
			return redirect()->to('/');
		}
	}
	public function vista_estadisticas_filtros($desde = null, $hasta = null)
	{
		if ($this->session->get('logged')) {
			$model = new Casos();
			//BUSCAMOS LOS CASOS ATENDIDOS POR RED SOCIAL
			$query_casos_atendidos = $model->contarCasosAtendidos_filtros($desde, $hasta);
			$estadisticas["Whatsapp"] = 0;
			$estadisticas["CorreoElectronico"] = 0;
			$estadisticas["Llamadatelefonica"] = 0;
			$estadisticas["Personal"] = 0;
			$estadisticas["Marcas"] = 0;
			$estadisticas["Patentes"] = 0;
			$estadisticas["DerechoAutor"] = 0;
			$estadisticas["Indicaciones_Geograficas"] = 0;
			$estadisticas["sexo_Marcas_M"] = 0;
			$estadisticas["sexo_Patentes_M"] = 0;
			$estadisticas["sexo_DerechoAutor_M"] = 0;
			$estadisticas["sexo_Indicaciones_Geograficas_M"] = 0;
			$estadisticas["sexo_Marcas_F"] = 0;
			$estadisticas["sexo_Patentes_F"] = 0;
			$estadisticas["sexo_DerechoAutor_F"] = 0;
			$estadisticas["sexo_Indicaciones_Geograficas_F"] = 0;
			$estadisticas["Sexo_CorreoElectronico_M"] = 0;
			$estadisticas["Sexo_Llamadatelefonica_M"] = 0;
			$estadisticas["Sexo_Whatsapp_M"] = 0;
			$estadisticas["Sexo_Personal_M"] = 0;
			$estadisticas["Sexo_CorreoElectronico_F"] = 0;
			$estadisticas["Sexo_Llamadatelefonica_F"] = 0;
			$estadisticas["Sexo_Whatsapp_F"] = 0;
			$estadisticas["Sexo_Personal_F"] = 0;
			$estadisticas["usuario"] = 0;
			$estadisticas["emprendedor"] = 0;
			//BUSCAMOS LOS CASOS ATENDIDOS POR TIPO BENEFICIARIO USUARIO
			$beneficiario_usuario = $model->contarCasos_Beneficiario_Usuario($desde, $hasta);
			if (!empty($beneficiario_usuario)) {
				for ($i = 0; $i < count($beneficiario_usuario); $i++) {
					if ($beneficiario_usuario[$i]->tipo_beneficiario == "1") {
						$estadisticas["usuario"] = $beneficiario_usuario[$i]->count;
					} else {
						$estadisticas[$beneficiario_usuario[$i]->tipo_beneficiario] = $beneficiario_usuario[$i]->count;
					}
				}
			}
			//BUSCAMOS LOS CASOS ATENDIDOS POR TIPO BENEFICIARIO EMPRENDERDOR
			$beneficiario_emprendedor = $model->contarCasos_Beneficiario_Emprendedor($desde, $hasta);
			if (!empty($beneficiario_emprendedor)) {
				for ($i = 0; $i < count($beneficiario_emprendedor); $i++) {
					if ($beneficiario_emprendedor[$i]->tipo_beneficiario == "2") {
						$estadisticas["emprendedor"] = $beneficiario_emprendedor[$i]->count;
					} else {
						$estadisticas[$beneficiario_emprendedor[$i]->tipo_beneficiario] = $beneficiario_emprendedor[$i]->count;
					}
				}
			}

			//BUSCAMOS LOS CASOS ATENDIDOS POR GENERO MASCULINO
			$query_casos_atendidos_Masculino = $model->contarCasosAtendidos_MASCULINO($desde, $hasta);
			if (!empty($query_casos_atendidos_Masculino)) {
				for ($i = 0; $i < count($query_casos_atendidos_Masculino); $i++) {
					if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Correo Electronico") {
						$estadisticas["Sexo_CorreoElectronico_M"] = $query_casos_atendidos_Masculino[$i]->count;
					} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Llamada telefonica") {
						$estadisticas["Sexo_Llamadatelefonica_M"] = $query_casos_atendidos_Masculino[$i]->count;
					} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Whatsapp") {
						$estadisticas["Sexo_Whatsapp_M"] = $query_casos_atendidos_Masculino[$i]->count;
					} else if ($query_casos_atendidos_Masculino[$i]->red_s_nom == "Personal") {
						$estadisticas["Sexo_Personal_M"] = $query_casos_atendidos_Masculino[$i]->count;
					} else {
						$estadisticas[$query_casos_atendidos_Masculino[$i]->red_s_nom] = $query_casos_atendidos_Masculino[$i]->count;
					}
				}
			}
			//BUSCAMOS LOS CASOS ATENDIDOS POR GENERO FEMENINO
			$query_casos_atendidos_Femenino = $model->contarCasosAtendidos_FEMENINO($desde, $hasta);
			if (!empty($query_casos_atendidos_Femenino)) {
				for ($i = 0; $i < count($query_casos_atendidos_Femenino); $i++) {
					if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Correo Electronico") {
						$estadisticas["Sexo_CorreoElectronico_F"] = $query_casos_atendidos_Femenino[$i]->count;
					} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Llamada telefonica") {
						$estadisticas["Sexo_Llamadatelefonica_F"] = $query_casos_atendidos_Femenino[$i]->count;
					} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Whatsapp") {
						$estadisticas["Sexo_Whatsapp_F"] = $query_casos_atendidos_Femenino[$i]->count;
					} else if ($query_casos_atendidos_Femenino[$i]->red_s_nom == "Personal") {
						$estadisticas["Sexo_Personal_F"] = $query_casos_atendidos_Femenino[$i]->count;
					} else {
						$estadisticas[$query_casos_atendidos_Femenino[$i]->red_s_nom] = $query_casos_atendidos_Femenino[$i]->count;
					}
				}
			}

			if (!empty($query_casos_atendidos)) {
				for ($i = 0; $i < count($query_casos_atendidos); $i++) {
					if ($query_casos_atendidos[$i]->red_s_nom == "Correo Electronico") {
						$estadisticas["CorreoElectronico"] = $query_casos_atendidos[$i]->count;
					} else if ($query_casos_atendidos[$i]->red_s_nom == "Llamada telefonica") {
						$estadisticas["Llamadatelefonica"] = $query_casos_atendidos[$i]->count;
					} else {
						$estadisticas[$query_casos_atendidos[$i]->red_s_nom] = $query_casos_atendidos[$i]->count;
					}
				}
				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL
				$query = $model->contarCasosPorPI_filtros($desde, $hasta);
				if (!empty($query)) {
					for ($i = 0; $i < count($query); $i++) {
						if ($query[$i]->tipo_prop_nombre == "Derecho de Autor") {
							$estadisticas["DerechoAutor"] = $query[$i]->count;
						} else if ($query[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
							$estadisticas["Indicaciones_Geograficas"] = $query[$i]->count;
						} else {
							$estadisticas[$query[$i]->tipo_prop_nombre] = $query[$i]->count;
						}
					}
				}
				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL GENERO MASCULINO
				$query_casos_MASCULINOS = $model->contarCasos_PI_MACULINO($desde, $hasta);
				for ($i = 0; $i < count($query_casos_MASCULINOS); $i++) {
					if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Derecho de Autor") {
						$estadisticas["sexo_DerechoAutor_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
						$estadisticas["sexo_Indicaciones_Geograficas_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Marcas") {
						$estadisticas["sexo_Marcas_M"] = $query_casos_MASCULINOS[$i]->count;
					} else if ($query_casos_MASCULINOS[$i]->tipo_prop_nombre == "Patentes") {
						$estadisticas["sexo_Patentes_M"] = $query_casos_MASCULINOS[$i]->count;
					} else {
						$estadisticas[$query_casos_MASCULINOS[$i]->tipo_prop_nombre] = $query_casos_MASCULINOS[$i]->count;
					}
				}
				//BUSCAMOS LOS CASOS ATENDIDOS POR PROPIEDAD INTELECTUAL GENERO FEMENINO
				$query_casos_FEMENINOS = $model->contarCasos_PI_FEMENINO($desde, $hasta);
				$estadisticas["sexo_Marcas_F"] = 0;
				$estadisticas["sexo_Patentes_F"] = 0;
				$estadisticas["sexo_DerechoAutor_F"] = 0;
				$estadisticas["sexo_Indicaciones_Geograficas_F"] = 0;
				for ($i = 0; $i < count($query_casos_FEMENINOS); $i++) {
					if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Derecho de Autor") {
						$estadisticas["sexo_DerechoAutor_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Indicaciones Geograficas") {
						$estadisticas["sexo_Indicaciones_Geograficas_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Marcas") {
						$estadisticas["sexo_Marcas_F"] = $query_casos_FEMENINOS[$i]->count;
					} else if ($query_casos_FEMENINOS[$i]->tipo_prop_nombre == "Patentes") {
						$estadisticas["sexo_Patentes_F"] = $query_casos_FEMENINOS[$i]->count;
					} else {
						$estadisticas[$query_casos_FEMENINOS[$i]->tipo_prop_nombre] = $query_casos_FEMENINOS[$i]->count;
					}
				}
				$fecha_desde = $desde;
				$fecha_hasta = $hasta; // Fecha en formato YYYY-MM-DD
				// Formatear la fecha al formato dd-mm-yy
				$fecha_formateada_desde = date("d-m-Y", strtotime($fecha_desde));
				$fecha_formateada_hasta = date("d-m-Y", strtotime($fecha_hasta));

				$estadisticas["fecha_desde"] = $fecha_formateada_desde;
				$estadisticas["fecha_hasta"] = $fecha_formateada_hasta;
			}
			//Pasamos la tabla como parametro para la vista
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('reportes/estadisticas/content.php', $estadisticas);
			echo view('template/footer');
			echo view('reportes/estadisticas/footer.php');
		} else {
			return redirect()->to('/');
		}
	}

	public function vista_estadisticas2()
	{
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('reportes/estadisticas/prueba.php');
		echo view('template/footer');
		echo view('reportes/estadisticas/footer.php');
	}
}
