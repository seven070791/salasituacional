<?php

namespace App\Models;

class Casos_remitidos_Model extends BaseModel
{


    //Metodo para insertar un nuevo caso en la BD
    public function remitirCaso(array $remitirCase)
    {
        $builder = $this->dbconn('sgc_casos_remitidos');
        $query = $builder->insert($remitirCase);
        return $query;
    }
}
