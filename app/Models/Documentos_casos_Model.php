<?php

namespace App\Models;

class Documentos_casos_Model extends BaseModel
{
    //Metodo que busca los ducumentos en funsion de los id de los casos 
    public function buscar_documentos_casos($idcaso = null)
    {

        $builder = $this->dbconn('sgc_documentos_casos');
        $builder->where('docu_id_caso=', $idcaso);
        $query = $builder->get();
        return $query;
        // $db      = \Config\Database::connect();
        // $strQuery = " SELECT docu.docu_id,docu.docu_ruta FROM public.sgc_documentos_casos as docu  ";
        // $strQuery .= " WHERE docu.docu_id_caso= '$idcaso'";
        // //return $strQuery;
        // $query = $db->query($strQuery);
        // $resultado = $query->getResult();
        // return $resultado;
    }
}
