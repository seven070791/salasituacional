<!-- Content Wrapper. Contains page content -->
<style>
  table.dataTable thead,
  table.dataTable tfoot {
    background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
  }
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/agregar_caso.css">
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 p-2">
          <div class="card">
            <div class="card-header border-0">

              <div class="d-flex justify-content-between">

                <h3 class="text-secondary"><i class="fas fa-angle-double-right"></i> AGREGAR CASO </h3>
              </div>
            </div>
            <div class="card-body">
              <!--Form-->
              <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 ">
                  <div class="card">

                    <form role="form" id="new-case" name="new-case">
                      <div class="card-body fondo">
                        <div class="form-group">
                          <div class="row">
                            <div class="form-group">
                              <div class="row">
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="nombre-persona">Nombre</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="nombre-persona" id="nombre-persona" onkeypress="noNumeros(event)" autocomplete="off" required>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="apellido-persona">Apellido</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="apellido-persona" id="apellido-persona" onkeypress="noNumeros(event)" autocomplete="off" required>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="tipo-persona">Tipo Persona</label>
                                  <select class="form-control" id="tipo-persona" name="tipo-persona">
                                    <option value="V">V - Venezolano</option>
                                    <option value="E">E - Extranjero</option>
                                    <option value="J">J - Jurifico</option>
                                    <option value="G">G - Gobierno</option>
                                  </select>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="cedula-persona">Nº cedula o Rif</label>
                                  <input type="text" class="form-control" name="cedula-persona" min="7" id="cedula-persona" autocomplete="off" required>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="t-beneficiario">Tipo de Beneficiario</label>
                                  <select class="form-control" id="t-beneficiario" name="t-beneficiario">
                                    <option value="1" selected>Usuario</option>
                                    <option value="2">Emprendedor</option>
                                  </select>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="tipo-persona">Genero</label>
                                  <select class="form-control" id="sexo" name="tipo-persona">
                                    <option value="1">Masculino</option>
                                    <option value="2">Femenino</option>
                                  </select>
                                </div>



                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="telefono-persona">Telefono</label>
                                  <input type="text" class="form-control" onkeypress="return valideKey(event);" name="telefono" id="telefono" autocomplete="off">
                                </div>
                                <div class="col-lg-2 col-sm-2 col-md-2">
                                  <label for="fecha-recibido">Fecha de Recibido</label>
                                  <input class="form-control" type="date" name="fecha-recibido" id="fecha-recibido" required>
                                </div>

                              </div>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-md-3">
                              <label for="red-social">Via de Atencion</label>
                              <select class="form-control" name="red-social" id="red-social">
                                <option value="0" disabled>Seleccione</option>
                              </select>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4">
                              <label for="">Atencion al Cuidadano</label>
                              <select class="form-control" name="office" id="office">
                                <option value="1" selected>Sala situacional</option>
                                <option value="2">Coordinacion Regional</option>
                              </select>
                            </div>
                            <div class="col-lg-5 col-sm-5 col-md-5">
                              <label for="correo">Correo Electronico</label>
                              <input type="email" class="form-control" name="correo" id="correo" autocomplete="off">
                            </div>
                          </div>

                        </div>
                        <div class="form-group">
                          <div class="row">

                            <div class="col-lg-12 col-sm-12 col-md-12">
                              <label for="direccion">Direccion</label>
                              <input type="text" class="form-control" name="direccion" id="direccion" autocomplete="off">
                            </div>
                          </div>
                          <br>
                          <div class="row">

                            <div class="col-4">
                              <label for="pais-caso">Pais</label>
                              <select id="pais-caso" disabled="disabled" name=" pais-caso" class="form-control">
                                <option value="1" selected>Venezuela</option>
                              </select>
                            </div>
                            <div class="col-4">
                              <label for="estado-caso">Estado</label>
                              <select id="estado-caso" name="estado-caso" class="form-control">
                                <option value="0" disabled>Seleccione Estado</option>

                              </select>
                            </div>
                            <div class="col-4">
                              <label for="municipio-caso">Municipio</label>
                              <select id="municipio-caso" name="municipio-caso" class="form-control">
                                <option value="0">Seleccione Municipio</option>
                              </select>
                            </div>

                            <div class="col-4">
                              <label for="parroquia-caso">Parroquia</label>
                              <select id="parroquia-caso" name="parroquia-caso" class="form-control">
                                <option value="0">Seleccione Parroquia</option>
                              </select>
                            </div>

                            <div class="col-lg-4 col-sm-4 col-md-4">
                              <label for="tipo-pi">Tipo Propiedad Intelectual</label>
                              <select class="form-control" id="tipo-pi" name="tipo-pi">
                                <option value="0" disabled>Seleccione</option>
                              </select>
                            </div>

                            <div class="col-lg-4 col-sm-4 col-md-4">
                              <label for="tipo-pi">Tipo de Atencion</label>
                              <select class="form-control" id="tipo-atencion-usu" name="tipo-atencioni-usu">
                                <option value="0" disabled>Seleccione</option>
                              </select>
                            </div>


                          </div>



                          <!-- FORMULARIO PARA EL CASO DE ASESORIA -->
                          <div class="row" id="cgr" style="display: none;">

                            <div class="col-lg-4 col-sm-4 col-md-4">
                              <label for="competancia-cgr">Ente asdcrito</label>
                              <select class="form-control" id="ente-adscrito" name="competencia-cgr" value="0">
                                <option value=" 0" selected disabled>Seleccione</option>
                              </select>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-md-3">
                              <label for="competancia-cgr">Competencia de CGR</label>
                              <select class="form-control" id="competencia-cgr" name="competencia-cgr" value="0">
                                <option value=" 0" selected disabled>Seleccione</option>
                                <option value="1">Si</option>
                                <option value="2">No</option>
                              </select>
                            </div>
                            <div class="col-lg-3 col-sm-3 col-md-3">
                              <label for="asume-cgr">Asume CGR</label>
                              <select class="form-control" id="asume-cgr" name="asume-cgr" value="0">
                                <option value="0" selected disabled>Seleccione</option>
                                <option value="1">Si</option>
                                <option value="2">No</option>
                              </select>
                            </div>
                          </div>
                          <!-- FORMULARIO PARA EL CASO DE DENUNCIAS -->
                          <div class="row" id="denuncias" style="display: none;">
                            <div class="col-lg-6">
                              &nbsp;&nbsp; <label for="asume-cgr">A quien afecta el hecho:</label>&nbsp;&nbsp;&nbsp;
                              <input type="radio" id="option-personal" name="option">&nbsp;&nbsp;&nbsp;
                              <span>Personal</span>&nbsp;&nbsp;&nbsp;
                              <input type="radio" id="option-comunidad" name="option">&nbsp;&nbsp;&nbsp;
                              <span>Comunidad</span>&nbsp;&nbsp;&nbsp;
                              <input type="radio" id="option-terceros" name="option">&nbsp;&nbsp;&nbsp;
                              <span>Terceros</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <label for="fecha-hechos">Fecha de los hechos</label>
                            <div class="col-lg-3">
                              <input class="form-control" type="date" name="fecha-hechos" id="fecha-hechos" value=" ">
                            </div>

                            <div class="col-10">
                              &nbsp;&nbsp;<label for="denu-involucrados">Indique Personas , Organismos o Instituciones Involucradas en los hechos :</label>
                              <textarea type="text" class="form-control" onkeyup="mayus(this);" name="denu-involucrados" id="denu-involucrados">
                                  </textarea>
                            </div>
                            <div class="col-11">
                              <br>
                              <label>EN CASO DE TRATARSE DE UNA INSTANCIA DEL PODER POPULAR INDIQUE :</label>
                              <div class=" row">
                                <div class="col-lg-5 col-sm-5 col-md-5">
                                  <label for="nombre-instancia">Nombre de la instancia del Poder Popular</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="nombre-instancia" id="nombre-instancia" autocomplete="off">
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="rif-instancia">Rif:</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="rif-instancia" id="rif-instancia" autocomplete="off">
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-4">
                                  <label for="ente-financiador">Ente Financiador:</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="ente-financiador" id="ente-financiador" autocomplete="off">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-5 col-sm-5 col-md-5">
                                  <label for="nombre-proyecto">Nombre del Proyecto:</label>
                                  <input type="text" class="form-control" onkeyup="mayus(this);" name="nombre-proyecto" id="nombre-proyecto" autocomplete="off">
                                </div>
                                <div class="col-lg-3 col-sm-3 col-md-3">
                                  <label for="monto-aprovado">Monto Aprobado:</label>
                                  <input type="text" class="form-control" onkeypress="return valideKey(event);" name="monto-aprovado" id="monto-aprovado" onkeypress="noNumeros(event)" autocomplete="off">
                                </div>
                              </div>

                            </div>

                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <label for="planteamiento-caso">Descripcion del Caso</label>
                            <textarea type="text" class="form-control" name="requerimiento-usuario" id="requerimiento-usuario" required>
                          </textarea>
                          </div>
                          <div>

                          </div>
                        </div>
                        <br />

                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn  btn-sm  btn-primary">Guardar</button>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

      <script type="text/javascript">
        function valideKey(evt) {
          var code = (evt.which) ? evt.which : evt.keyCode;
          if (code == 8) { // backspace.
            return true;
          } else if (code >= 48 && code <= 57) { // is a number.
            return true;
          } else { // other keys.
            return false;
          }
        }
      </script>
      <!-- ***** FUNCION PARA SOLO LETRAS***-** -->
      <script>
        function noNumeros(event) {
          const tecla = event.keyCode || event.which;
          if (tecla >= 48 && tecla <= 57) {
            event.preventDefault();
          }
        }
      </script>
      <!-- ***** FUNCION PARA CONVERTIR EN MAYUSCULA***-** -->
      <script>
        function mayus(e) {
          e.value = e.value.toUpperCase();
        }
      </script>


      <script>
        var textArea = document.getElementById("requerimiento-usuario");
        textArea.addEventListener("click", function() {
          this.selectionStart = 0;
          this.selectionEnd = 0;
        });
      </script>