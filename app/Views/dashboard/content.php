<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/botones_datatable.css">
<style>
  table.dataTable thead,
  table.dataTable tfoot {
    background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
    ;
  }
</style>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <!-- /.row -->
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 p-2">
          <div class="card">
            <div class="card-header border-0">
              <div class="d-flex justify-content-between">
                <h3 class="text-secondary"><i class="fas fa-angle-double-right"></i> Ultimos 20 casos registrados por el Usuario</h3>
              </div>
            </div>
            <div class="card-body">
              <!--Form-->
              <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12 ">
                  <div class="card">

                    <div class="card-body">
                      <table class="display" id="table_casos" style="width:100%" style="margin-top: 20px">
                        <thead>
                          <tr>
                            <!-- <td class="text-center" style="width: 1%;">NºCaso</td> -->
                            <td class="text-center" style="width: 15%;">Nombre y Apellido del Usuario</td>
                            <td class="text-center" style="width: 5%;">Telefono</td>
                            <td class="text-center" style="width: 10%;">Propiedad Intelectual</td>
                            <td class="text-center" style="width: 5%;">Tipo Atencion</td>
                            <td class="text-center" style="width: 5%;">F.Recepcion</td>
                            <td class="text-center" style="width: 3%;">Estatus </td>

                          </tr>
                        </thead>
                        <tbody id="listar_casos">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>