<!-- Content Wrapper. Contains page content -->


<script src="https://cdn.jsdelivr.net/npm/chart.js@latest/dist/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>

<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Estadisticas Globales</h1>
        </div>
        <div class="col-sm-6">
          &nbsp;&nbsp; <label for="min">Desde</label>&nbsp;
          <input type="date" class="bodersueve" style="width:140px;" value="<?php echo date('YY-MM-DD'); ?>" name="desde" id="desde">&nbsp;&nbsp;
          <label for="hasta">Hasta</label>&nbsp;&nbsp;
          <input type="date" class="bodersueve" style="width:140px;" value="<?php echo date('YY-MM-DD'); ?>" name="hasta" id="hasta">&nbsp;
          &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-primary consultar">Consultar</button>
          &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-secondary limpiar">Limpiar</button>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="card">
      <form id="anual-report" name="anual-report" method="POST" class="form-horizontal">
        <!-- /.card -->
        <div class="row">
          <div class="col-6">

  </section>

  <a href="#" id="downloadPdf">Download Report Page as PDF</a>
  <br /><br />
  <div id="reportPage">
    <div id="chartContainer" style="width: 30%;float: left;">
      <canvas id="myChart"></canvas>
    </div>

    <div style="width: 30%; float: left;">
      <canvas id="myChart2"></canvas>
    </div>

    <br /><br /><br />

    <div style="width: 30%; height: 400px; clear: both;">
      <canvas id="myChart3" style="width: 40%"></canvas>
    </div>
  </div>
  <!-- /.content-wrapper -->

  options: {
  responsive: true,
  title: {
  display: true,
  text: "Chart.js - Changing X Axis Step Size"
  },
  tooltips: {
  mode: "index",
  intersect: false
  },
  legend: {
  display: false
  },
  scales: {
  xAxes: [{
  ticks: {
  beginAtZero: true,
  stepSize: 2
  }
  }]
  }
  }