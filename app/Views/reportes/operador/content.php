<!-- Content Wrapper. Contains page content -->
<?php
$session = session();
?>

<div class="content-wrapper">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/botones_datatable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/reportes.css">
  <style>
    table.dataTable thead,
    table.dataTable tfoot {
      background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
    }
  </style>
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 p-2">
          <div class="card">
            <div class="card-header border-0">
              <div class="d-flex justify-content-between">
                <h3 class="text-secondary"><i class="fas fa-angle-double-right"></i> Reporte por Analistas &nbsp;&nbsp;<select class="custom-select " id="usuarios" style="width:200px;" name="direcciones_caso" class="form-control">
                    <option value="0" selected disabled>seleccione</option>
                    <?php echo $usuarios; ?>
                  </select> </h3>
              </div>
            </div>
            <!--Form-->

            <input name="typeReport" id="typeReport" type="hidden" value="1">
            <div class="row">
              <div class="col-md-12">
                &nbsp;&nbsp; <label for="min">Desde</label>&nbsp;
                <input type="date" class="bodersueve" style="width:120px;" value="<?php echo date('YY-MM-DD'); ?>" name="desde" id="desde">&nbsp;
                <label for="hasta">Hasta</label>&nbsp;&nbsp;
                <input type="date" class="bodersueve" style="width:120px;" value="<?php echo date('YY-MM-DD'); ?>" name="hasta" id="hasta">&nbsp;
                &nbsp;&nbsp;
                &nbsp;&nbsp; <label class="labelsexo">Genero</label>&nbsp;
                <select class="custom-select " style="width:130px;" id="sexo" name="sexo" data-style="btn-primary">
                  <option value="0" selected disabled>seleccione</option>
                  <option value="1">MASCULIINO</option>
                  <option value="2">FEMENINO</option>
                </select>
                <label class="labeltipo-pi">Tipo de Propiedad Int</label>&nbsp;
                <select class="custom-select " style="width:220px;" id="tipo-pi" name="tipo-pi" data-style="btn-primary">
                  <option value="0" selected disabled>seleccione</option>
                </select>&nbsp;&nbsp;

                <label class="labelvia-atencion-usu">Via de Atencion</label>&nbsp;
                <select class="custom-select " style="width:145px;" id="via-atencion" name="via-atencion" data-style="btn-primary">
                  <option value="0" selected disabled>seleccione</option>
                </select>
              </div>
              <div>
                &nbsp;&nbsp;&nbsp;&nbsp; <label class="labeltipo-atencion-usu">Tipo.Atencion</label>&nbsp;
                <select class="custom-select " style="width:145px;" id="tipo-atencion-usu" name="tipo-atencion-usu" data-style="btn-primary">
                  <option value="0" selected disabled>seleccione</option>
                </select>&nbsp;&nbsp;
                <label for="t-beneficiario">Tipo.Beneficiario</label>
                <select class="custom-select " style="width:145px;" id="t-beneficiario" name="t-beneficiario">
                  <option value="0" selected disabled>seleccione</option>
                  <option value="1">Usuario</option>
                  <option value="2">Emprendedor</option>
                </select>
                &nbsp;&nbsp; <label for="direcciones_caso">Direcciones Administrativa</label>
                <select class="custom-select " id="direcciones_caso" style="width:300px;" name="direcciones_caso" class="form-control">

                  <option value="0" selected disabled>seleccione</option>
                  <?php echo $direcciones; ?>
                </select>
                &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-primary consultar">Consultar</button>&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;<button type="button" class="btn btn-sm btn-secondary limpiar">Limpiar</button>

              </div>
              <div class="col-4">
              </div>
              <div class="col-2">
                <div class="p-3"></div>

              </div>

            </div>



          </div>
        </div>
      </div>
      <!--Form-->
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 ">
          <div class="card">
            <div class="card-body">
              <table class="display" id="table_casos" style="width:100%" style="margin-top: 20px">
                <thead>
                  <tr>
                    <!-- <td class="text-center" style="width: 1%;">Nº</td> -->
                    <td class="text-center" style="width: 1%;">Cedula</td>
                    <td class="text-center" style="width: 1%;">Tipo Ben</td>
                    <td class="text-center" style="width: 12%;">Beneficiario</td>
                    <td class="text-center" style="width: 3%;">Telefono</td>
                    <td class="text-center" style="width: 6%;">P.Intelectual</td>
                    <td class="text-center" style="width: 4%;">T.Atencion</td>
                    <td class="text-center" style="width: 1%;">Fecha</td>
                    <td class="text-center" style="width: 1%;">Estatus</td>
                    <td class="text-center" style="width: 12%;">Direccion Remitida</td>
                    <td class="text-center" style="width: 5%;">User</td>
                  </tr>
                </thead>
                <tbody id="listar_casos">
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>