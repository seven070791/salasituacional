<?php
$session = session();
?>
<meta charset="utf-8">
<link rel="stylesheet" href="<?php echo base_url(); ?>/css_paginas/navar.css">
<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-whiFte navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>


      </ul>

      <input type="text" name="" disabled="disabled" style="width: 410px; background-color: transparent; border: none;" value="">
      <h5><span class="brand-text" style="display: flex; justify-content: center; align-items: center; color: white; font-weight: bold;">SISTEMA INTEGRADO DE ATENCION AL CIUDADANO</span></h5>





      <ul class="navbar-nav ml-auto  ">

        <!-- <a href="javascript:history.back()"><img src="<php echo base_url();?>/img/i2.jpg" class="img-size-50 img-circle mr-3"></a> -->
        <li class="nav-item">
        <li class="nav-item d-none d-sm-inline-block">
          <h5><a href="#Foo" onclick="cerrarSesion();" style="color: white;" class="nav-primary">Salir</a></h5>

        </li>
        <script>
          function cerrarSesion() {
            Swal.fire({
              title: '¿Deseas salir?',
              text: 'Estás a punto de abandonar la página.',
              icon: 'question',
              showCancelButton: true,
              confirmButtonText: 'Sí',
              cancelButtonText: 'No'
            }).then((result) => {
              if (result.value) {
                // Aquí puedes agregar la lógica para salir de la página
                window.location.href = '/';
              }
            });
          }
        </script>
        </li>
      </ul>
    </nav>
    <aside class="main-sidebar   sidebar-dark-light elevation-4" id="aside" data-toggle="collapse">
      <!-- Brand Logo -->
      <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">SIAC </span>
        <img src="<?php echo base_url(); ?>/img/LogoSIAC_sapi2.png" alt="AdminLTE Logo" class="brand-image" style="opacity: .8; float: left; box-shadow: none; width: 50px;" onclick="return false;">
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">

          <div class="info" style="text-align: center;">
            <a href="#" class="d-block">
              <h6><?= session('nombre'); ?></h6>
            </a>
          </div>
        </div>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu1">
          <li class="nav-item ">
            <a href="<?php echo base_url(); ?>/inicio" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Inicio </p>
            </a>
          </li>
          <?php if ($session->get('userrol') == 1) { ?>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/adminUsers" class="nav-link" id="casos"><i class="nav-icon fas fa-copy" id="menu_citas"></i>
                <p>Gestion de Usuario</p>
                <i class="right fas fa-angle-left"></i>
              </a>

              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/adminUsers" class="nav-link">
                    <i class="nav-icon 	fas fa-address-card" style='font-size:20px'></i>
                    <p>Usuarios</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/auditoria_sistema" class="nav-link">
                    <i class="nav-icon 	fas  fa-users" style='font-size:20px'></i>
                    <p>Auditoria</p>
                  </a>
                </li>
              </ul>
            </li>

          <?php } ?>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>/casos" class="nav-link" id="casos"><i class="nav-icon fas fa-copy" id="menu_citas"></i>
              <p>Casos</p>

            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>/reposos" class="nav-link" id="menu_reposos"><i class="nav-icon fas fa-copy"></i>
              <p> Reportes</p>
              <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>/consolidado" class="nav-link">
                  <i class="nav-icon 	 fa fa-book" style='font-size:20px'></i>
                  <p>Consolidado</p>
                </a>
              </li>
              <?php if ($session->get('userrol') == 1) { ?>
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/operador" class="nav-link">
                    <i class="nav-icon 	fas  fa-users" style='font-size:20px'></i>
                    <p>Analista</p>
                  </a>
                </li>
              <?php } ?>

              <?php if ($session->get('userrol') == 1) { ?>

                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/estadisticas" class="nav-link">
                    <i class="fas fa-chart-bar" style='font-size:20px'></i>
                    <p>Estadisticas</p>
                  </a>
                </li>
              <?php } ?>


            </ul>

          </li>

          <?php if ($session->get('userrol') == 1) { ?>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/reposos" class="nav-link" id="menu_reposos"><i class="nav-icon fas fa-copy"></i>
                <p> Mantenimiento</p>
                <i class="right fas fa-angle-left"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/vista_direcciones_admin" class="nav-link">
                    <i class="nav-icon 	 fa fa-book" style='font-size:20px'></i>
                    <p>Direcciones adminis</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/vista_tipo_atencion" class="nav-link">
                    <i class="nav-icon 	fas  fa-users" style='font-size:20px'></i>
                    <p>Tipo De Atencion</p>
                  </a>
                </li>
              </ul>
            </li>
          <?php } ?>
          <li class="nav-item">
            <a href="#Foo" onclick="cerrarSesion();" class="nav-link" id="casos"><i class="fa fa-external-link" aria-hidden="true"></i>
              <p>Salir</p>

            </a>
          </li>

      </div>
    </aside>
</body>