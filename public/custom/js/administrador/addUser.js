$(function() {
    listar_usuarios();
    llenar_combo_roles(Event, id);
});
//FUNCION PARA LLENAR EL COMBO DE LOS ROLES 
function llenar_combo_roles(e, id) {
    e.preventDefault
    url = '/listar_Combo_Roles';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#edit-user-rol').empty();
                $('#edit-user-rol').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#edit-user-rol').append('<option value=' + item.idrol + '>' + item.rolnom + '</option>');
                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.idrol === id) {
                            $('#edit-user-rol').append('<option value=' + item.idrol + ' selected>' + item.rolnom + '</option>');

                        } else {

                            $('#edit-user-rol').append('<option value=' + item.idrol + '>' + item.rolnom + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}
/*
 * Función para definir datatable de usuarios:
 */
function listar_usuarios() {
    $('#table_usuarios').DataTable({
        "order": [
            [0, "asc"]
        ],
        "paging": true,
        "info": true,
        "filter": true,
        "responsive": true,
        "autoWidth": true,
        //"stateSave":true,
        "ajax": {
            "url": "/Get_All_Usuarios",
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            { data: 'idusuopr' },
            { data: 'usuopnom' },
            { data: 'usuopape' },
            { data: 'rolnom' },
            { data: 'usuopemail' },
            { data: 'usercargo' },

            {
                orderable: true,
                render: function(data, type, row) {
                    if (row.usuopborrado == 'f') {
                        return '<span id="btnalerta"class="  btnactivo "disabled="disabled">' + 'Activo' + '</span>'
                    } else {
                        return '<span id="btnsolvente"class="btninnactivo "disabled="disabled">' + 'Innactivo' + '</span>'

                    }

                }
            },

            {
                orderable: true,
                render: function(data, type, row) {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"     usuoppass=' + row.usuoppass + '   usuopemail="' + row.usuopemail + '" idusuopr=' + row.idusuopr + ' usuopnom="' + row.usuopnom + '" usuopape=' + row.usuopape + ' idrol=' + row.idrol + '  borrado=' + row.usuopborrado + ' usercargo="' + row.usercargo + '" > <i class="material-icons " >create</i></a>' + ' ' +
                        '<a href="javascript:;" class="btn btn-xs btn-light Bloquear" style=" font-size:1px" data-toggle="tooltip" title="Bloquear" usuoppass=' + row.usuoppass + '   usuopemail="' + row.usuopemail + '" idusuopr=' + row.idusuopr + ' usuopnom="' + row.usuopnom + '" usuopape=' + row.usuopape + ' idrol=' + row.idrol + '> <i class="material-icons " >delete</i > < /a>'
                }
            }
        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }],
        }
    });
}


//Verificacion si las dos contraseñas son iguales al agregar
$(document).on('keyup', "#user-confirm-pass", (e) => {
    e.preventDefault();
    let pass = $("#user-confirm-pass").val();
    if ($("#user-pass").val() != pass) {
        $("#user-pass").addClass('is-invalid');
        $("#user-confirm-pass").addClass('is-invalid');
        $("button[type=submit]").attr('disabled', 'true');
    } else {
        $("#user-pass").removeClass('is-invalid');
        $("#user-confirm-pass").removeClass('is-invalid');
        $("#user-pass").addClass('is-valid');
        $("#user-confirm-pass").addClass('is-valid');
        $("button[type=submit]").removeAttr('disabled');
    }
});

// //EVENTO PARA AGREGAR UN USUARIO
$(document).on('submit', "#new-user", function(e) {
    e.preventDefault();

    let datos = {
        "username": $("#user-name").val(),
        "userlastname": $("#user-lastname").val(),
        "useremail": $("#user-email").val(),
        "userrol": $("#user-rol").val(),
        "userpass": $("#user-pass").val(),
        "usercargo": $("#usercargo").val()
    }
    $.ajax({
        url: "/addNewUser",
        method: "POST",
        dataType: "JSON",
        data: {
            "data": btoa(JSON.stringify(datos))
        },
        beforeSend: function() {
            // $("button[type=submit]").attr('disabled', "true");
        },

        success: function(mensaje) {

            if (mensaje === 0) {
                Swal.fire({
                    icon: "error",
                    type: 'error',
                    html: '<strong>Error!! El Usuario ya Existe</strong>',
                    toast: true,
                    position: "center",
                    showConfirmButton: false,
                    //timer: 3500,

                });
                setTimeout(function() {
                    window.location = "/adminUsers";
                }, 1500);
            } else if (mensaje === 1) {
                Swal.fire({
                    icon: "success",
                    type: 'success',
                    html: '<strong>Usuario Registrado Exitosamente</strong>',
                    toast: true,
                    position: "center",
                    showConfirmButton: false,
                    //timer: 1500,
                });
                setTimeout(function() {
                    window.location = "/adminUsers";
                }, 1500);
            } else if (mensaje === 2) {
                Swal.fire({
                    icon: "error",
                    type: 'error',
                    html: '<strong>Ocurrio un error al registrar el Usuario/strong>',
                    toast: true,
                    position: "center",
                    showConfirmButton: false,
                    //timer: 1500,
                });
                setTimeout(function() {
                    window.location = "/adminUsers";
                }, 1500);
            }
        }
    });
});



//METODO PARA ABRIR EL MODAL PARA LA   EDICION
$('#listar_usuarios').on('click', '.Editar', function(e) {
    var idusuopr = $(this).attr('idusuopr');
    var usuopnom = $(this).attr('usuopnom');
    var usuopape = $(this).attr('usuopape');
    var usuopemail = $(this).attr('usuopemail');
    var usuoppass = $(this).attr('usuoppass');
    var usuopborrado = $(this).attr('borrado');
    var usercargo = $(this).attr('usercargo');
    if (usercargo == 'null') {
        usercargo = ''
    }
    var id = $(this).attr('idrol');
    llenar_combo_roles(e, id);
    $("#editUser").modal("show");
    $('#editUser').find('#edit-user-name').val(usuopnom);
    $('#editUser').find('#edit-user-lastname').val(usuopape);
    $('#editUser').find('#edit-user-email').val(usuopemail);
    $('#editUser').find('#userid').val(idusuopr);
    $('#editUser').find('#edit-user-pass').val(usuoppass);
    $('#editUser').find('#edit-user-confirm-pass').val(usuoppass);
    $('#editUser').find('#clave-anterior').val(usuoppass);
    $('#editUser').find('#cargo').val(usercargo);
    estatus_borrado = false;
    if (usuopborrado == 'f') {
        $('#usuopborrado').attr('checked', 'checked');
        $('#usuopborrado').val('false');
    }
    if (usuopborrado == 't') {
        $('#usuopborrado').removeAttr('checked')
        $('#usuopborrado').val('true')
    }
});

//******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
$('#usuopborrado').click(function() {
    if ($('#usuopborrado').is(':checked')) {

        estatus_borrado = 'false';

    } else {
        estatus_borrado = 'true';

    }
});

let cambiar_clave = 'false';
$('#cambiar-clave').click(function() {
    if ($('#cambiar-clave').is(':checked')) {

        $('#modulo-claves').show();
        cambiar_clave = 'true';

    } else {
        cambiar_clave = 'false';
        $('#modulo-claves').hide();

    }
});


// //Evento para guardar el usuario editado
$(document).on('submit', "#edit-user", function(e) {
    e.preventDefault();
    let clave_actual = $("#edit-user-pass").val();
    let clave_anterior = $("#edit-user-confirm-pass").val();
    let usercargo = $("#cargo").val();
    if (cambiar_clave == 'true') {
        clave_actual = clave_actual.trim();
        if (clave_anterior == clave_actual) {
            $("#edit-user-pass").addClass('is-invalid');
            alert('ERROR! LA CONTRASEÑA DEBE SER DIFERENTE A LA ANTERIOR')

        } else {
            $("#edit-user-pass").removeClass('is-invalid');
            $("#edit-user-confirm-pass").removeClass('is-invalid');
            let datos = {
                "username": $("#edit-user-name").val(),
                "userlastname": $("#edit-user-lastname").val(),
                "useremail": $("#edit-user-email").val(),
                "userrol": $("#edit-user-rol").val(),
                "userid": $("#userid").val(),
                "usercargo": usercargo,
                "usuoppass": clave_actual,
                "usuopborrado": estatus_borrado,
                "modulo_clave": 'true',
            }
            $.ajax({
                url: "/editUser",
                method: "POST",
                dataType: "JSON",
                data: {
                    "data": btoa(JSON.stringify(datos))
                },
                beforeSend: function() {
                    $("button[type=submit]").attr('disabled', "true");
                }
            }).then((response) => {
                Swal.fire('Exito!', "Usuario editado exitosamente", "success");
                $("#editUser").modal('hide');
                $("button[type=submit]").removeAttr('disabled');
                setTimeout(function() {
                    window.location = '/adminUsers/';
                }, 1500);
            }).catch((request) => {
                Swal.fire("Error!", "Ha ocurrido un error", "error");
                $("button[type=submit]").removeAttr('disabled');
                setTimeout(function() {
                    window.location = '/adminUsers/';
                }, 1600);
            });

        }

    } else {
        let datos = {
            "username": $("#edit-user-name").val(),
            "userlastname": $("#edit-user-lastname").val(),
            "useremail": $("#edit-user-email").val(),
            "userrol": $("#edit-user-rol").val(),
            "userid": $("#userid").val(),
            "usuopborrado": estatus_borrado,
            "usercargo": usercargo,
            "modulo_clave": 'false',
        }


        $.ajax({
            url: "/editUser",
            method: "POST",
            dataType: "JSON",
            data: {
                "data": btoa(JSON.stringify(datos))
            },
            beforeSend: function() {
                $("button[type=submit]").attr('disabled', "true");
            }
        }).then((response) => {
            Swal.fire('Exito!', "Usuario editado exitosamente", "success");
            $("#editUser").modal('hide');
            $("button[type=submit]").removeAttr('disabled');
            setTimeout(function() {
                window.location = '/adminUsers/';
            }, 1500);
        }).catch((request) => {
            Swal.fire("Error!", "Ha ocurrido un error", "error");
            $("button[type=submit]").removeAttr('disabled');
            setTimeout(function() {
                window.location = '/adminUsers/';
            }, 1600);
        });
    }
})

//METODO PARA BLOQUEAR UN USUARIO
$('#listar_usuarios').on('click', '.Bloquear', function(e) {
    var idusuopr = $(this).attr('idusuopr');
    let usuopborrado = 'true'
    let datos = {
        idusuopr: idusuopr,
        usuopborrado: usuopborrado

    }
    Swal.fire({
        title: '¿Deseas Bloquear el Registro?',
        text: 'El registro será bloqueado del Sitema.',
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.value) {
            // Aquí puedes agregar la lógica para salir de la página
            $.ajax({
                url: "/Bloquear_User",
                method: "POST",
                dataType: "JSON",
                data: {
                    "data": btoa(JSON.stringify(datos))
                },
                beforeSend: function() {

                }
            }).then((response) => {
                Swal.fire('Exito!', "Usuario Bloqueado exitosamente", "success");
                $("#editUser").modal('hide');

                setTimeout(function() {
                    window.location = '/adminUsers/';
                }, 1500);
            }).catch((request) => {
                Swal.fire("Error!", "Ha ocurrido un error", "error");
                setTimeout(function() {
                    window.location = '/adminUsers/';
                }, 1600);
            });

        }
    });
});